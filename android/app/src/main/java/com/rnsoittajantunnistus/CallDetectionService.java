package com.rnsoittajantunnistus;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.Nullable;

import com.facebook.react.HeadlessJsTaskService;
import com.facebook.react.bridge.Promise;

public class CallDetectionService extends Service {

    private TelephonyManager telephonyManager;
    private Context mContext;
    private boolean ignoreSavedNumbers;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String CHANNEL_ID = "CallDetectionService";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_ID,
                    NotificationManager.IMPORTANCE_LOW);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);
            Notification notification = new Notification.Builder(mContext, CHANNEL_ID)
                    .setContentTitle("Running CallDetectionService")
                    .setContentText(mContext.getPackageName())
                    .build();
            startForeground(1, notification);
        }

        telephonyManager = (TelephonyManager) mContext.getSystemService(
                Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ignoreSavedNumbers = intent.getBooleanExtra("ignoreSavedNumbers",true);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final PhoneStateListener phoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            if (state == TelephonyManager.CALL_STATE_RINGING) {
                if(ignoreSavedNumbers && isNumberInContacts(incomingNumber))
                    return;
                Intent headlessIntent = new Intent(mContext, HeadlessEventService.class);
                headlessIntent.putExtra("number", incomingNumber);
                mContext.startService(headlessIntent);
                HeadlessJsTaskService.acquireWakeLockNow(mContext);
            }
        }
    };

    /**
     * Checks if provided phone number is saved in contacts
     * @param phoneNumber MANDATORY phone number to check
     * @return true if numbers is in contacts
     */
    private Boolean isNumberInContacts(String phoneNumber) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        Cursor cursor = this.mContext.getContentResolver().query(uri,
                new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);

        return (cursor.getCount() > 0);
    }
}