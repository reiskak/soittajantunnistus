package com.rnsoittajantunnistus;

import android.content.Intent;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import javax.annotation.Nonnull;

public class CallDetectionModule extends ReactContextBaseJavaModule {
    public static final String REACT_CLASS = "CallDetection";
    private static ReactApplicationContext reactContext;

    public CallDetectionModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        CallDetectionModule.reactContext = reactContext;
    }

    @Nonnull
    @Override
    public String getName() {
        return REACT_CLASS;
    }

    /**
     * startService
     * Starts CallDetectionService, that monitors incoming phonecalls and sends the
     * phone number to the javascript side with HeadlessJS
     * 
     * @param ignoreSavedNumbers MANDATORY sets if numbers saved in contacts should
     *                           be sent to headless service.
     */
    @ReactMethod
    public void startService(Boolean ignoreSavedNumbers) {
        Intent intent = new Intent(this.reactContext, CallDetectionService.class);
        intent.putExtra("ignoreSavedNumbers", ignoreSavedNumbers);
        CallDetectionModule.reactContext.startService(intent);
    }
}