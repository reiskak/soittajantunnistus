/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { handleIncomingCall } from './src/callDetection'

AppRegistry.registerHeadlessTask("CallDetection", () => handleIncomingCall);

AppRegistry.registerComponent(appName, () => App);



