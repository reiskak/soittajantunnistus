import React from 'react';
import type { Node } from 'react';
import {
  SafeAreaView,
  Text,
  View,
  useEffect,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { requestPermissions } from './src/helpers/requestPermissions';
import { startDetectionService } from './src/callDetection'
import History from './src/components/History';





const App: () => Node = () => {
  React.useEffect(() => {
    requestPermissions();
    startDetectionService(true);

  }, []);



  return (
    <SafeAreaView>
      <View style={{ justifyContent: "center", flexDirection: "row" }}>
        <Text style={{ fontWeight: "bold", fontSize: 25 }}>Soittajantunnistus</Text>
      </View>
      <History />
    </SafeAreaView>
  );
};

export default App;
