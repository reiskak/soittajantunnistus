import {
    PermissionsAndroid
} from 'react-native';


/**requestPermissions
 * requests android permissions needed for the app.
 */
export const requestPermissions = async () => {

    await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
    )

    await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
    )

    await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CALL_LOG,
    )


}
