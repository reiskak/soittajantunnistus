import notifee, { AndroidImportance, AndroidStyle } from '@notifee/react-native';

/**displayNotification
 * displays notification for provided data.
 * @param {object} messages MANDATORY conatins fonecta and kukasoitti fields containing string for notification 
 */
export async function displayNotification(messages) {
    // Create a channel
    const channelId = await notifee.createChannel({
        id: 'Soittajantunnistus',
        name: 'Soittajantunnistus Channel',
        importance: AndroidImportance.HIGH,
    });

    // Display a notification
    await notifee.displayNotification({
        title: messages.fonecta,
        body: messages.kukasoitti,
        android: {
            channelId,
            importance: AndroidImportance.HIGH,
            style: {
                type: AndroidStyle.INBOX,
                lines: messages.kukasoittiComments ?? []
            },
        },
    });
}