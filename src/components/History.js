import { StyleSheet, Text, View, FlatList } from 'react-native';
import React, { useEffect, useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const History = () => {

    const [historyData, setHistoryData] = useState([]);
    const [refreshing, setRefreshing] = useState(false);
    importData = async () => {
        try {
            const keys = await AsyncStorage.getAllKeys();
            const result = await AsyncStorage.multiGet(keys);
            const history = result.map(req => { return { ...JSON.parse(req[1]), number: req[0] } }).sort((a, b) => b.added - a.added);
            setHistoryData(history);
        } catch (error) {
            console.error(error)
        }
        setRefreshing(false);
    }

    useEffect(() => {
        importData();
    }, []);

    const HistoryItem = ({ numberData }) => {
        return (
            <View style={{ padding: 10 }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: "bold" }}>{numberData.number}</Text>
                    <Text style={{ marginLeft: "auto", fontWeight: "bold" }}>{(new Date(numberData.added)).toLocaleDateString('fi-FI')}</Text>
                </View>

                <Text>{numberData.fonecta}</Text>
                <Text>{numberData.kukasoitti}</Text>
            </View>
        )
    }

    return (
        <View>
            <Text style={{ padding: 10, fontWeight: "bold", fontSize: 20 }} >Hakuhistoria:</Text>
            <FlatList
                keyExtractor={numberData => numberData.number}
                data={historyData}
                renderItem={({ item }) => <HistoryItem numberData={item} />}
                onRefresh={() => {
                    setRefreshing(true);
                    importData();
                }}
                refreshing={refreshing}
            />
        </View>
    );
};

export default History;

const styles = StyleSheet.create({});
