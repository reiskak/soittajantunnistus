import {
    NativeModules,
} from 'react-native';
import NumberSearchAPI from "./api/numbersearch"
import { displayNotification } from './helpers/notifications';
import AsyncStorage from '@react-native-async-storage/async-storage';

const { CallDetection } = NativeModules;

/**startDetectionService
 * Starts foreground service for calldetection, service triggers CallDetection headless task on incoming calls
 * @param {boolean} ignoreSavedNumbers MANDATORY specifies if saved numbers should be ignored
 */
export const startDetectionService = (ignoreSavedNumbers: boolean) => {
    CallDetection.startService(ignoreSavedNumbers);
}

/**handleIncomingCall
 * Checks incoming number with NumberSearchAPI and shows notification containing the information
 * @param {*} taskData MANDATORY provided by headless task, should contain number field containing callers number 
 */
export const handleIncomingCall = async (taskData) => {
    const { number } = taskData;
    try {
        const value = await AsyncStorage.getItem(number)
        if (value !== null) {
            await displayNotification(JSON.parse(value));
            return;
        }
    } catch (error) {
        console.log("Failed to access AsyncStorage: " + error.message)
    }

    NumberSearchAPI.getNumberInfo(number).then(async res => {
        await displayNotification(res);
        if (!res.error) {
            try {
                const jsonValue = JSON.stringify({ ...res, added: Date.now() })
                await AsyncStorage.setItem(number, jsonValue)
            } catch (error) {
                console.log("Failed to save to AsyncStorage: " + error.message)
            }
        }
    });
}