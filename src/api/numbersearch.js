import conf from "../../conf"
import axios from "axios";
import cheerio from "cheerio";

class NumberSearchAPI {
    #login;
    #password;
    #fonectaBearer = '';
    #fonectaExpire = 0;

    #fonectaHeaders = {
        "Referer": "https://www.fonecta.fi/",
        "Origin": "https://www.fonecta.fi",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0"
    };


    constructor() {
        this.#login = conf.fonectaAccount;
        this.#password = conf.fonectaPassword;
    }

    /**getNumberInfo
     * Gets info about provided phonumber from fonecta and kukasoitti services
     * @param {string} phoneNumber MANDATORY phonenumber to search for
     * @returns {object} containing fields kukasoitti and fonecta, that contain string information about number and kukasoittiComments field containing array of strings.
     */
    getNumberInfo = async (phoneNumber) => {
        if (Date.now() > this.#fonectaExpire) {
            await this.#fonectaLogin();
            console.log("auth done");
        }

        const promises = [
            axios.get('https://www.kukasoitti.fi/numero/' + phoneNumber),
            axios.get('https://qbi8ved60c.execute-api.eu-west-1.amazonaws.com/prod/api/v2/search?what=' + phoneNumber + '&sort=RELEVANCE&contact_type=ALL&page=1&newSearch=true', { headers: { ...this.#fonectaHeaders, "Authorization": this.#fonectaBearer } })
        ]
        const promisesResolved = promises.map(promise => promise.catch(error => ({ error })))

        let self = this;
        const res = await axios.all(promisesResolved).then(axios.spread(function (res1, res2) {
            const fonecta = self.#parseFonecta(res2);
            const kukasoitti = self.#parseKukasoitti(res1);
            return { ...fonecta, ...kukasoitti };
        })).catch((err) => {
            console.log(err);
            return { fonecta: "error", kukasoitti: "error", error: true, kukasoittiComments: [] };
        });
        return res;
    }

    /**fonectaLogin
     * authenticates to fonecta and updates fonectaBearer and fonectaExpire needed for fonectas number search
     */
    #fonectaLogin = async () => {
        const headers = {
            "Referer": "https://www.fonecta.fi/",
            "Origin": "https://www.fonecta.fi",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0"
        }
        const data = {
            'appId': 'fonecta.fi',
            'email': this.#login,
            'password': this.#password,
            'sourceAppId': null,
            'sourceAppReturnPath': null
        }
        await axios.post('https://tili.fonecta.fi/modalLogin/login', data, {
            headers: this.#fonectaHeaders
        })
            .then(async (response) => {
                await axios.get(response.data.data.redirUrl, {
                    withCredentials: true,
                    headers: this.#fonectaHeaders
                }).then((res) => {
                    this.#regexToken(res.headers);
                }).catch((err) => {
                    console.log(err)
                });

            })
            .catch((error) => {
                console.log(error.response)
            });
    }
    /**regexToken
     * parses the auth token from fonectas response headers.
     * @param {*} cookies MANDATORY cookies from fonectas auth response
     */
    #regexToken = (cookies) => {
        var patt = /fofi_user=s%3A(.*?)\./;
        var match = cookies["set-cookie"][0].match(patt);
        this.#fonectaBearer = 'Bearer ' + match[1];
        this.#fonectaExpire = Date.now() + 7150000;
    }

    #parseKukasoitti = (axiosResponse) => {
        if (axiosResponse.error)
            return {
                kukasoitti: "Kukasoitti: 404",
                kukasoittiComments: []
            }
        else {
            let data = axiosResponse.data;
            const $ = cheerio.load(data);
            let kukasoittiString = "Kukasoitti: Vaara:" + $('#progress-bar-inner').text() + "% Katselut: " + $('.tooltip').first().text();
            let kukasoittiComments = [kukasoittiString];
            $(".comment-item  ").each((i, element) => {
                kukasoittiComments.push($(element).find('.rank').first().text() + ": " + $(element).find('.comment-text').first().text());
            });

            return {
                kukasoitti: kukasoittiString,
                kukasoittiComments: kukasoittiComments
            };
        }
    }

    #parseFonecta = (axiosResponse) => {
        if (axiosResponse.error)
            return {
                fonecta: "Fonecta: errored"
            };
        else {
            if (axiosResponse.data.data.totalResults < 1)
                return {
                    fonecta: "Fonecta: Ei tuloksia"
                };
            else {
                return { fonecta: "Fonecta: " + axiosResponse.data.data.contacts[0].name };
            }
        }
    }


}


export default new NumberSearchAPI();